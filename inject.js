/*
    Extension-Website Communication Code
*/

window.addEventListener("message", function(event) {
  // We only accept messages from ourselves
  if (event.source != window)
    return;

  //check what functios are to be called and execute accordingly.
  if (event.data.type && (event.data.type == "leap")) {
    console.log("Content script received: " + event.data.text);
    switch(event.data.text){
        
        case "l":
            navl()
            break;
        
        case "r":
            navr();
            break;
        
        case "cc":
            zoomIn();
            break;
        
        case "ca":
            zoomOut();
            break;
        
        case "t":
            play();
            break;
        
        case "u":
            present();
            break;
        
        default:
            console.log("default ran")
    
    }

  }
});
                        

/*
    Extension-website Communication Code End
*/