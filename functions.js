
//Keydown event generator. s - shift, c - control, a - alt. Passed bool values to determine if they're a part of the event
Podium = {};
Podium.keydown = function(k, s, c, a) {
    var oEvent = document.createEvent('KeyboardEvent');
    var shift = "Shift"
    var alt = "Alt"
    var ctrl = "Control"

    array = [s,a,c]
    mod = ""

    //create modifier string to pass into event
    for (var i =0; i< array.length; i++){
        if (array[i] == true && i==0) 
            mod = mod.concat(shift + " ")
        else if (array[i] == true && i==1) 
            mod = mod.concat(alt + " ")
        else if (array[i] == true && i==2)
            mod = mod.concat(ctrl)
        else{}//do nothing
            
    }

    // Chromium Hack
    Object.defineProperty(oEvent, 'keyCode', {
                get : function() {
                    return this.keyCodeVal;
                }
    });     
    Object.defineProperty(oEvent, 'which', {
                get : function() {
                    return this.keyCodeVal;
                }
    });     

    if (oEvent.initKeyboardEvent) {
        oEvent.initKeyboardEvent("keydown", true, true, document.defaultView, k, k, "", "", false, mod);
    } else {
        oEvent.initKeyEvent("keydown", true, true, document.defaultView, false, false, false, false, k, 0);
    }

    oEvent.keyCodeVal = k;

    if (oEvent.keyCode !== k) {
        alert("keyCode mismatch " + oEvent.keyCode + "(" + oEvent.which + ")");
    }
    
    document.dispatchEvent(oEvent);
}



/*
    START OF FUNCTIONS
*/

//functions that dispatch keydown events to the leap motion script in the DOM 

//control changing slides
//left
function navl(){
    Podium.keydown(37,false,false,false);
    console.log("navl called")
}

//right
function navr(){
    Podium.keydown(39,false,false,false);
    console.log("navr called")
    
}

//zooming out
//no/poor results in presenter mode. Entirely based on the application and not the extension
function zoomIn(){
    Podium.keydown(107,false,true,false);
    console.log("zoom in called")
}

//zooming in
//no/poor results in presenter mode. Entirely based on the application and not the extension
function zoomOut(){
    Podium.keydown(109,false,true,false);
    console.log("zoom out called")
}
    
//start presentation
//the app isnt taking the keystroke event, whether programmatic or manual
function present(){
    Podium.keydown(116,true,true,false);
    console.log("present called")
}

//play video or move to next slide
function play(){
    Podium.keydown(13,false,false,false);
    console.log("play called")
}


/*
    END OF FUNCTIONS
*/
