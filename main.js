/*
    HANDLING GESTURE FUNCTIONS
*/

//swipes
function swipeHandle(Gesture){
   
    var isHorizontal = Math.abs(Gesture.direction[0]) > Math.abs(Gesture.direction[1]);
    //Classify as right-left or up-down
    if(isHorizontal){
        
        if(Gesture.direction[0] > 0){
            window.postMessage({ type: "leap", text: "l" }, "*");
            
        }
        else {
            window.postMessage({ type: "leap", text: "r" }, "*");
        }
    }
    
    // its vertical
    else { 
        if(Gesture.direction[1] > 0) {
            window.postMessage({ type: "leap", text: "u" }, "*"); //start presentation
        }
    }
}

//circles
function circleHandle(Gesture, Frame){
    
    //determine if clockwise or not
    var clockwise = false;
    var pointableID = Gesture.pointableIds[0];
    var direction = Frame.pointable(pointableID).direction;
    var dotProduct = Leap.vec3.dot(direction, Gesture.normal);
    
    if (dotProduct  >  0) clockwise = true;
    
    if (clockwise) window.postMessage({ type: "leap", text: "cc" }, "*");
    
    else window.postMessage({ type: "leap", text: "ca" }, "*");
}

/*
    END OF HANDLING GESTURE FUNCTIONS
*/

//let app work on fullscreen only
//var isFullScr = false



var port = chrome.runtime.connect(); //opens port to listen for instructions from extension

/*
    LEAP MOTION CODE
*/



var id = null //tracking gestures by ID
var circleNum = 0 //number of circles drawn 

var controller = new Leap.Controller({frameEventName: 'deviceFrame', enableGestures : 'true'});

controller.connect();
controller.on('frame', onFrame);

function onFrame(frame) {

    
    /*
        HANDLING TOOL DATA FROM FRAME 
    */
    
    //drawable when fullscreen
    if( window.innerHeight == screen.height) {
    
        // sets up injected canvas in fullscreen
        var canvas = document.getElementById("leap");
        var ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        
        //gets position of pointable (if any) and puts it onscreen
        if (frame.tools.length === 1){
            console.log(frame)
            for(var t = 0; t < frame.tools.length; t++) {
                var position = frame.tools[t].stabilizedTipPosition;
                var normalized = frame.interactionBox.normalizePoint(position);

                var x = ctx.canvas.width * normalized[0];
                var y = ctx.canvas.height * (1 - normalized[1]);
                ctx.fillStyle="#0000FF"
                ctx.beginPath();
                ctx.rect(x, y, 10, 10);
                ctx.fill();
            }
    
        }
    }
    
    /*
        GESTURES
    */
    
    //skip frame if multiple gestures are recognised
    if (frame.gestures.length === 1)  {

        gesture = frame.gestures[0]
        
        //check if new gesture is old or not
        if (id === gesture.id || gesture.state === "update") {

            //if the gesture is completed, wait a second before taking new gesture
            if (gesture.state === "stop") {
                setTimeout(function(){}, 1000);
                if (gesture.type === "circle") setTimeout(function(){console.log("wait before accepting gestures again")},2000); //prevent mistakes after circle 
                circleNum = 0 //reser circle number
            }

            //deal with circles to ensure they are completed before calling function
            else if (gesture.state === "update" && gesture.type === "circle" && gesture.pointableIds.length === 1){

                // true if circle made complete rotation
                var isComplete = (Math.floor(gesture.progress)) > circleNum

                //make sure circle progress is at least one complete circle
                if (isComplete) {
                    //call function and update circle value
                    circleHandle(gesture, frame);
                    circleNum = gesture.progress
                }
            }
        }

        //make sure gesture is valid before continuing
        else if (id !== gesture.id && gesture.pointableIds.length === 1 && gesture.state !== "update"){

            //save gesture id
            id = gesture.id

            //deal with screen tap gestures

            //determine type of gesture and respond accordingly
            //circles are handled differently, with progress
            switch (gesture.type) {

                case "swipe" :
                    swipeHandle(gesture);
                    break;

                case "screenTap" :
                    window.postMessage({ type: "leap", text: "t" }, "*");
                    break;    

                default:
                    console.log("disposed of useless gesture")
                    break;
            }


        }


        else {
            //console.log("disposed of useless gesture")
            //ignore. useless data
        }
    }
}


/*
    JS INJECTION
*/

var script = document.createElement("script");
script.src = chrome.extension.getURL("jquery-1.11.1.min.js");
document.head.appendChild(script); 

script = document.createElement("script");
script.src = chrome.extension.getURL("functions.js");
document.head.appendChild(script); 

//add canvas to the DOM
var canvas = document.createElement("canvas");
canvas.setAttribute("style", "position : absolute;");
canvas.setAttribute("id", "leap");
document.body.appendChild(canvas);

//add communication code to the DOM
script = document.createElement("script");
script.src = chrome.extension.getURL("inject.js");
document.head.appendChild(script);
/*
    JS INJECTION END
*/